//distance betweeen two points.

#include<stdio.h>
#include<math.h>

int main()

{
    int c,d,e,f,h,x1,x2,y1,y2;
    float g;
    
    
    printf("x1-axis and y1-axis:");
    scanf("%d %d",&x1,&y1);
    
    printf("x2-axis and y2-axis:");
    scanf("%d %d",&x2,&y2);
    
    c=x2-x1;
    d=y2-y1;
    
    e=pow(c,2);
    f=pow(d,2);
    
    g=sqrt(e+f);
    
    printf("distance between two point:%f",g);
    
    return 0;
    
    
    
}